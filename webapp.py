import socket



class WebApp:

    def parse(self, request):
        request_parsed = request.split(" ")
        return request_parsed[0] , request_parsed[1]


    def process(self, parsed_request):
        return "200 OK", "<html><body><h1>hola mundo</h1></body></html>"

    def __init__(self, address, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Af_INET es igual a ipv4 (adrees famili)

        # deja que use el puerto si no lo esta usando nadie
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # socket escuha
        s.bind((address, port))  # si dejamos el campo vacio se pueden conectar tanto de localhost comode ip

        # socket escucha#cuantas peticiones acepto
        s.listen(5)

        # aceptar peticiones
        while True:

            (rec_socket, address) = s.accept()  # c es el tubo de comunicacion
            # leo un limite , hasta 2 KB
            received = rec_socket.recv(2048)
            print(address, received)

            recieved_str = received.decode("utf-8")
            request_data = self.parse(recieved_str)

            codigo , response_body = self.process(request_data)

            response  = f"HTTP:/1.1 {codigo} \r\n\r\n {response_body} \r\n"
            rec_socket.send(response.encode("utf-8"))
            rec_socket.close






if __name__ == "__main__":
    webApp("", 6789)
