import socket
import webapp
pages = {
    "/": "<html><body></h1>Bienvenido a la pagina , pon / y un numero del 1 al 5 .</h1></body></html>",
    "/1": "<html><body></h1>Bienvenido a la pagina 1</h1></body></html>",
    "/2": "<html><body><h1>Welcome to page 2</h1></body></html>",
    "/3": "<html><body><h1>benvenuti a pagina 3</h1></body></html>",
    "/4": "<html><body><h1>bienvenue à la page 4</h1></body></html>",
    "/5": "<html><body><h1>üdvözöljük a 5. oldalon</h1></body></html>",
}


class ContentApp(webapp.WebApp):


    def process(self, parsed_request):
        if parsed_request[1] in pages:
            return "200 OK", pages[parsed_request[1]]

        else:
            return "404 Not Found", "<html><body><h1> 404 Not Found! </h1></body></html>"



if __name__ == "__main__":
    ContentApp("", 6789)
